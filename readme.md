# Container for building the PlayStation 2 KernelLoader 3.0 Homebrew

This is the Dockerfile / containerfile that will create a working build environment for KernelLoader 3.0.

This is based upon the instructions here: https://web.archive.org/web/20231013022507/https://www.ps2-home.com/forum/viewtopic.php?t=13062

This is required because the source looks to have been last touched about 10-15 years ago, depending on where you look.

This uses my internal Debian 7 container image, but that's just

`debootstrap --no-check-gpg --merged-usr --variant=minbase --include=bzip2 wheezy http://archive.debian.org/debian`

All of the sources for kernelloader, it's libraries and toolchains are included in this repo; internet connection is only required for the debian packages.

## Building this container

This should build like any other dockerfile / containerfile:

```
podman build -f containerfile [-t <an OCI url that you might push this to>]
```

(The -t argument is optional)

## Usage

**Note:** I use podman here, but most of the commands should work with docker if you just swap the names.

A copy of the kernelloader 3 sources are included in `/root/kernelloader-3.0`, so if you just want to build it (or test that the container works), run the container, cd into that folder and run make:

```
podman run -it <the hash or tag of this container>
cd /root/kernelloader3.0
make
[...]
sjcrunch loader.elf kloader.elf
SjCRUNCH Packer v2.1

Packing...
Linking...
Done!
rm mcicons/icon_sys.s TGE/dmarelay_irx.s SMSUTILS_irx.s mcicons/kloader_icn.s SMSCDVD_irx.s usb_mass_irx.s ps2kbd_irx.s poweroff_irx.s cloud_rgb.s selected_rgb.s disc_rgb.s unselected_rgb.s ps2http_irx.s mcicons/icon.sys ps2ip_irx.s back_rgb.s penguin_rgb.s ps2smap_irx.s fileXio_irx.s ioptrap_irx.s TGE/intrelay-direct_irx.s ps2dev9_irx.s iomanX_irx.s TGE/sbios_old_elf.s up_rgb.s smaprpc_irx.s TGE/sbios_new_elf.s TGE/intrelay-dev9_irx.s TGE/intrelay-direct-rpc_irx.s dns_irx.s mcicons/kloader.icn eromdrvloader_irx.s sharedmem_irx.s usbd_irx.s eedebug_irx.s folder_rgb.s TGE/intrelay-dev9-rpc_irx.s kernel_elf.s ps2link_irx.s
make[1]: Leaving directory `/root/kernelloader3.0/loader'
```

In this case, `/root/kernelloader3.0/loader/loader.elf` is the fully built ELF, and `../kloader.elf` is the "packed" (executable compression) version.

If you want to build from source that's already on your computer, then use `-v <source dir>:/mnt`, and add make on the end since it starts in /mnt by default:

```
podman run -it -v <source dir>:/mnt <hash or tag> make
[... build log here ...]
```
